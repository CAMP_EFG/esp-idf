// Copyright 2015-2017 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CA_API /* ESP API */

#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_partition.h"
#include "esp_spiffs.h"
#include "esp_vfs.h"
#include "spiffs_api.h"

static const char* TAG = "SPIFFS";

void spiffs_api_lock(spiffs *fs)
{
    (void) xSemaphoreTake(((esp_spiffs_t *)(fs->user_data))->lock, portMAX_DELAY);
}

void spiffs_api_unlock(spiffs *fs)
{
    xSemaphoreGive(((esp_spiffs_t *)(fs->user_data))->lock);
}

s32_t spiffs_api_read(spiffs *fs, uint32_t addr, uint32_t size, uint8_t *dst)
{
    esp_err_t err = esp_partition_read(((esp_spiffs_t *)(fs->user_data))->partition, 
                                        addr, dst, size);
    if (unlikely(err)) {
        ESP_LOGE(TAG, "failed to read addr %08x, size %08x, err %d", addr, size, err);
        return -1;
    }
    return 0;
}

s32_t spiffs_api_write(spiffs *fs, uint32_t addr, uint32_t size, uint8_t *src)
{
    esp_err_t err = esp_partition_write(((esp_spiffs_t *)(fs->user_data))->partition, 
                                        addr, src, size);
    if (unlikely(err)) {
        ESP_LOGE(TAG, "failed to write addr %08x, size %08x, err %d", addr, size, err);
        return -1;
    }
    return 0;
}

s32_t spiffs_api_erase(spiffs *fs, uint32_t addr, uint32_t size)
{
    esp_err_t err = esp_partition_erase_range(((esp_spiffs_t *)(fs->user_data))->partition, 
                                        addr, size);
    if (err) {
        ESP_LOGE(TAG, "failed to erase addr %08x, size %08x, err %d", addr, size, err);
        return -1;
    }
    return 0;
}

void spiffs_api_check(spiffs *fs, spiffs_check_type type, 
                            spiffs_check_report report, uint32_t arg1, uint32_t arg2)
{
    static const char * spiffs_check_type_str[3] = {
        "LOOKUP",
        "INDEX",
        "PAGE"
    };

    static const char * spiffs_check_report_str[7] = {
        "PROGRESS",
        "ERROR",
        "FIX INDEX",
        "FIX LOOKUP",
        "DELETE ORPHANED INDEX",
        "DELETE PAGE",
        "DELETE BAD FILE"
    };

    if (report != SPIFFS_CHECK_PROGRESS) {
        ESP_LOGE(TAG, "CHECK: type:%s, report:%s, %x:%x", spiffs_check_type_str[type], 
                              spiffs_check_report_str[report], arg1, arg2);
    } else {
        ESP_LOGV(TAG, "CHECK PROGRESS: report:%s, %x:%x", 
                              spiffs_check_report_str[report], arg1, arg2);
    }
}

#else /* CA_API */

#include "osa.h"
#include "phal_nvm_dfns.h"
#include "phal_fs.h"
#include "spiffs_api.h"
#include "spiffs/src/spiffs.h"
#include "spiffs/src/spiffs_nucleus.h"

/*! @defgroup SPIFFS_API
 * @ingroup SPIFFS
 * @{
 */
/* *************************
 *  Defines
 * *************************/
#define SPIFFS_FLASH_SIZE         (1*1024*1024) /* Reserve trailling 1MB for secure boot.
                                                 *
                                                 * Time (second) of formatting and mounting fresh filsystem
                                                 *            Mount           Format
                                                 * 1-MB           1                7
                                                 * 2-MB           2               13
                                                 * 4-MB           3               26
                                                 * 6-MB           5               38
                                                 * 8-MB           7               51
                                                 * 10-MB          9               64
                                                 * 12-MB         10               77
                                                 * 14-MB         12               90
                                                 * 15-MB         13               96
                                                 *
                                                 * Tested with commit 9092fcdf.
                                                 *
                                                 * Note that mounting time might increase after some time of usage.
                                                 */
#define SPIFFS_PHYS_ADDR          (0x300000)
#define SPIFFS_SECTOR_SIZE        (4096)
#define SPIFFS_ERASE_SIZE         (4096)
#define SPIFFS_LOG_BLOCK_SIZE     (32 * SPIFFS_SECTOR_SIZE)
#define SPIFFS_LOG_PAGE_SIZE      (256)
#define SPIFFS_NUM_FD             (8)
#define SPIFFS_NUM_CACHE_PAGES    (4)
#define SPIFFS_UNUSED             (0u)
#define SPIFFS_FORCE_FORMAT_FILE   "__force_format__"
#define SPIFFS_ERR_FORCE_FORMAT   (-90000)

typedef struct
{
  spiffs spiffs;
  uint32_t mount_time_ms;
  uint32_t format_time_ms;
  bool just_formatted;
} spiffs_api_t;

static spiffs_api_t _spiffs;
static u8_t _spiffs_work_buf[SPIFFS_LOG_PAGE_SIZE * 2];
static u8_t _spiffs_fds[SPIFFS_NUM_FD * sizeof(spiffs_fd)];
#if SPIFFS_CACHE
static u8_t _spiffs_cache_buf[sizeof(spiffs_cache) +
                                                   SPIFFS_NUM_CACHE_PAGES *
                                                   (sizeof(spiffs_cache_page) + SPIFFS_LOG_PAGE_SIZE)];
#endif

static int32_t ts_subtract(struct timespec *result, const struct timespec *x, const struct timespec *y)
{
  if ((NULL == result) || (NULL == x) || (NULL == y))
  {
    return 0;
  }

  if ((x->tv_nsec - y->tv_nsec) < 0)
  {
    result->tv_sec = x->tv_sec - y->tv_sec - 1;
    result->tv_nsec = ORDER_9 + x->tv_nsec - y->tv_nsec;
  }
  else
  {
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_nsec = x->tv_nsec - y->tv_nsec;
  }

  /* Return 1 if result is negative. */
  return (x->tv_sec < y->tv_sec) ? 1 : 0;
}

/*!
 * \brief Flash read wrapper function
 */
static int32_t _spiffs_sf_read(uint32_t addr, uint32_t size, uint8_t *dst)
{
  if (size > UINT16_MAX)
  {
    return SPIFFS_ERR_INTERNAL;
  }

  phal_nvm_rw_t data;
  data.flash.addr = addr;
  data.flash.buf = dst;
  data.flash.len = (uint16_t)size;
  int32_t ret = phal_nvm_read((const char *)PHAL_NVM_TYPE_SPIFLASH, &data);
  usleep(500);

  return (ret == OSA_SUCCESS) ? SPIFFS_OK : SPIFFS_ERR_INTERNAL;
}


/*!
 * \brief Flash write wrapper function
 */
static int32_t _spiffs_sf_write(uint32_t addr, uint32_t size, uint8_t *src)
{
  if (size > UINT16_MAX)
  {
    return SPIFFS_ERR_INTERNAL;
  }

  phal_nvm_rw_t data;
  data.flash.addr = addr;
  data.flash.buf = src;
  data.flash.len = (uint16_t)size;
  int32_t ret = phal_nvm_write((const char *)PHAL_NVM_TYPE_SPIFLASH, &data);
  usleep(1000);

  return (ret == OSA_SUCCESS) ? SPIFFS_OK : SPIFFS_ERR_INTERNAL;
}


/*!
 * \brief Flash erase wrapper function
 */
static int32_t _spiffs_sf_erase(u32_t addr, u32_t size)
{
  if (size > SPIFFS_ERASE_SIZE)
  {
    return SPIFFS_ERR_INTERNAL;
  }

  phal_nvm_del_t data;
  data.flash.addr = addr;
  data.flash.type = PHAL_NVM_SECTOR;
  int32_t ret = phal_nvm_erase((const char *)PHAL_NVM_TYPE_SPIFLASH, &data);
  usleep(1000);

  return (ret == OSA_SUCCESS) ? SPIFFS_OK : SPIFFS_ERR_ERASE_FAIL;
}

/*!
 * \brief  Calculate physical path length
 *
 * @param[in] fs_api - the fs_api handle
 * @param[in] path - logical path
 *
 * @return physical path length
 */
static uint32_t _spiffs_physical_path_length(const struct _fs_api_t *fs_api, const uint8_t *path)
{
  uint32_t length = (uint32_t)strlen((const char *)path);

  if (fs_api->physical_root)
  {
    length += (uint32_t)strlen((const char *)fs_api->physical_root);
  }

  return length;
}

/*!
 * \brief  Get physical path
 *
 * @param[in] fs_api - the fs_api handle
 * @param[in] path - logical path
 * @param[out] actual_path - buffer to store physical path
 * @param[in] size - the size of \ref actual_path
 *
 * @return actual path length if success
 */
static int32_t _spiffs_physical_path(const struct _fs_api_t *fs_api, const uint8_t *path,
                                     uint8_t *actual_path, uint32_t size)
{
  if (fs_api->physical_root)
  {
    return snprintf((char *)actual_path, size, "%s%s", (const char *)fs_api->physical_root, path);
  }

  return snprintf((char *)actual_path, size, "%s", path);
}

/*!
 * \brief RTOS implementation of \ref fs_api_t.open
 */
static int32_t _spiffs_api_open(const struct _fs_api_t *fs_api, const uint8_t *path, int32_t oflag)
{
  spiffs_flags actual_oflag = 0;

  switch (oflag & PHAL_FS_FILE_O_ACCMODE)
  {
    case PHAL_FS_FILE_O_RDONLY:
      actual_oflag = SPIFFS_O_RDONLY;
      break;

    case PHAL_FS_FILE_O_WRONLY:
      actual_oflag = SPIFFS_O_WRONLY;
      break;

    case PHAL_FS_FILE_O_RDWR:
      actual_oflag = SPIFFS_O_RDWR;
      break;

    default:
      return PHAL_FS_ERR;
  }

  if (oflag & PHAL_FS_FILE_O_CREAT)
  {
    actual_oflag |= SPIFFS_O_CREAT;
  }

  if (oflag & PHAL_FS_FILE_O_EXCL)
  {
    actual_oflag |= SPIFFS_O_EXCL;
  }

  if (oflag & PHAL_FS_FILE_O_TRUNC)
  {
    actual_oflag |= SPIFFS_O_TRUNC;
  }

  if (oflag & PHAL_FS_FILE_O_APPEND)
  {
    actual_oflag |= SPIFFS_O_APPEND;
  }

  if (oflag & PHAL_FS_FILE_O_DIRECT)
  {
    actual_oflag |= SPIFFS_O_DIRECT;
  }

  uint32_t actual_path_length = _spiffs_physical_path_length(fs_api, path);
  uint8_t actual_path[actual_path_length + 1];
  _spiffs_physical_path(fs_api, path, actual_path, (uint32_t)sizeof(actual_path));

  spiffs_file fd = SPIFFS_open((spiffs *)fs_api->fs_handle, (const char *)actual_path, actual_oflag, SPIFFS_UNUSED);

  if (fd < 0)
  {
    return PHAL_FS_ERR;
  }

  return (int32_t)fd;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.close
 */
static int32_t _spiffs_api_close(const struct _fs_api_t *fs_api, int32_t fd)
{
  s32_t ret = SPIFFS_close((spiffs *)fs_api->fs_handle, (spiffs_file) fd);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.read
 */
static int32_t _spiffs_api_read(const struct _fs_api_t *fs_api, int32_t fd, void *buf, uint32_t nbytes)
{
  return SPIFFS_read((spiffs *)fs_api->fs_handle, (spiffs_file) fd, buf, nbytes);
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.write
 */
static int32_t _spiffs_api_write(const struct _fs_api_t *fs_api, int32_t fd, const void *buf, uint32_t nbytes)
{
  return SPIFFS_write((spiffs *)fs_api->fs_handle, (spiffs_file) fd, (void *)buf, nbytes);
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.lseek
 */
static int32_t _spiffs_api_lseek(const struct _fs_api_t *fs_api, int32_t fd, int32_t offset, int32_t whence)
{
  /* SPIFFS_SEEK_SET, SPIFFS_SEEK_CUR, and SPIFFS_SEEK_END happen to be equal to
   * FILE_API_SEEK_SET, FILE_API_SEEK_CUR, FILE_API_SEEK_END respectively.
   */
  return SPIFFS_lseek((spiffs *)fs_api->fs_handle, (spiffs_file) fd, offset, whence);
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.stat
 */
static int32_t _spiffs_api_stat(const struct _fs_api_t *fs_api, const uint8_t *path, phal_fs_stat_t *st)
{
  uint32_t actual_path_length = _spiffs_physical_path_length(fs_api, path);
  uint8_t actual_path[actual_path_length + 1];
  _spiffs_physical_path(fs_api, path, actual_path, (uint32_t)sizeof(actual_path));
  spiffs_stat fstate = {0};
  s32_t ret = SPIFFS_stat((spiffs *)fs_api->fs_handle, (const char *)actual_path, &fstate);
  st->size = (int32_t)fstate.size;
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.fstat
 */
static int32_t _spiffs_api_fstat(const struct _fs_api_t *fs_api, int32_t fd, phal_fs_stat_t *st)
{
  spiffs_stat fstate = {0};
  s32_t ret = SPIFFS_fstat((spiffs *)fs_api->fs_handle, (spiffs_file) fd, &fstate);
  st->size = (int32_t)fstate.size;
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.fsync
 */
static int32_t _spiffs_api_fsync(const struct _fs_api_t *fs_api, int32_t fd)
{
  s32_t ret = SPIFFS_fflush((spiffs *)fs_api->fs_handle, (spiffs_file) fd);
  return (0 <= ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.unlink
 */
static int32_t _spiffs_api_unlink(const struct _fs_api_t *fs_api, const uint8_t *path)
{
  uint32_t actual_path_length = _spiffs_physical_path_length(fs_api, path);
  uint8_t actual_path[actual_path_length + 1];
  _spiffs_physical_path(fs_api, path, actual_path, (uint32_t)sizeof(actual_path));
  s32_t ret = SPIFFS_remove((spiffs *)fs_api->fs_handle, (const char *)actual_path);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.truncate
 */
static int32_t _spiffs_api_truncate(const struct _fs_api_t *fs_api, const uint8_t *path, int32_t length)
{
  uint32_t actual_path_length = _spiffs_physical_path_length(fs_api, path);
  uint8_t actual_path[actual_path_length + 1];
  _spiffs_physical_path(fs_api, path, actual_path, (uint32_t)sizeof(actual_path));
  s32_t ret = SPIFFS_truncate((spiffs *)fs_api->fs_handle, (const char *)actual_path, length);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.fstat
 */
static int32_t _spiffs_api_ftruncate(const struct _fs_api_t *fs_api, int32_t fd, int32_t length)
{
  s32_t ret = SPIFFS_ftruncate((spiffs *)fs_api->fs_handle, (spiffs_file) fd, length);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.rename
 */
static int32_t _spiffs_api_rename(const struct _fs_api_t *fs_api, const uint8_t *oldpath,
                                  const uint8_t *newpath)
{
  uint32_t actual_oldpath_length = _spiffs_physical_path_length(fs_api, oldpath);
  uint8_t actual_oldpath[actual_oldpath_length + 1];
  _spiffs_physical_path(fs_api, oldpath, actual_oldpath, (uint32_t)sizeof(actual_oldpath));

  uint32_t actual_newpath_length = _spiffs_physical_path_length(fs_api, newpath);
  uint8_t actual_newpath[actual_newpath_length + 1];
  _spiffs_physical_path(fs_api, newpath, actual_newpath, (uint32_t)sizeof(actual_newpath));

  s32_t ret = SPIFFS_rename((spiffs *)fs_api->fs_handle, (const char *)actual_oldpath,
                            (const char *)actual_newpath);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * \brief RTOS implementation of \ref fs_api_t.factory_reset
 */
static int32_t _spiffs_api_factory_reset(const struct _fs_api_t *fs_api)
{
  spiffs_DIR d;
  struct spiffs_dirent e;
  struct spiffs_dirent *pe = &e;
  int32_t res;

  spiffs_file fd = -1;

  if (NULL == SPIFFS_opendir((spiffs *)fs_api->fs_handle, (const char *)fs_api->physical_root, &d))
  {
    return PHAL_FS_ERR;
  }

  while (SPIFFS_readdir(&d, pe))
  {
    // found one
    fd = SPIFFS_open_by_dirent((spiffs *)fs_api->fs_handle, pe, SPIFFS_RDWR, 0);

    if (fd < 0)
    {
      return PHAL_FS_ERR;
    }

    res = SPIFFS_fremove((spiffs *)fs_api->fs_handle, fd);

    if (res < 0)
    {
      return PHAL_FS_ERR;
    }

    res = SPIFFS_close((spiffs *)fs_api->fs_handle, fd);

    if (res < 0)
    {
      return PHAL_FS_ERR;
    }
  }

  return (0 == SPIFFS_closedir(&d)) ? PHAL_FS_SUCCESS : PHAL_FS_ERR;
}


/*!
 * @see spiffs_api.h
 */
int32_t spiffs_api_mount(void)
{
  spiffs_config cfg;
#if (!SPIFFS_SINGLETON)
  cfg.phys_size = SPIFFS_FLASH_SIZE;
  cfg.phys_addr = SPIFFS_PHYS_ADDR;
  cfg.phys_erase_block = SPIFFS_SECTOR_SIZE;
  cfg.log_block_size = SPIFFS_LOG_BLOCK_SIZE;
  cfg.log_page_size = SPIFFS_LOG_PAGE_SIZE;
#endif

  cfg.hal_read_f = _spiffs_sf_read;
  cfg.hal_write_f = _spiffs_sf_write;
  cfg.hal_erase_f = _spiffs_sf_erase;

  struct timespec mount_time_start = {0};
  struct timespec mount_time_end = {0};
  struct timespec format_time_start = {0};
  struct timespec format_time_end = {0};
  struct timespec result;
  clock_gettime(CLOCK_REALTIME, &mount_time_start);
  /* It takes about 7 seconds to mount a 15MB filesystem */
#if SPIFFS_CACHE
  s32_t ret = SPIFFS_mount(&_spiffs.spiffs, &cfg, _spiffs_work_buf, _spiffs_fds,
                           sizeof(_spiffs_fds), _spiffs_cache_buf, sizeof(_spiffs_cache_buf), 0);
#else
  s32_t ret = SPIFFS_mount(&_spiffs.spiffs, &cfg, _spiffs_work_buf, _spiffs_fds,
                           sizeof(_spiffs_fds), NULL, 0, 0);
#endif
  /* TODO: this is for development only, exclude this from release builds. */
  if (SPIFFS_OK == ret)
  {
    spiffs_stat stat;
    ret = SPIFFS_stat(&_spiffs.spiffs, SPIFFS_FORCE_FORMAT_FILE, &stat);

    if (SPIFFS_OK == ret)
    {
      /* Secret file exists, perform formatting. */
      spiffs_api_unmount();
      ret = SPIFFS_ERR_FORCE_FORMAT;
    }
    else
    {
      /* No formatting requests. */
      ret = SPIFFS_OK;
    }
  }

  switch (ret)
  {
    case SPIFFS_ERR_NOT_A_FS:
    case SPIFFS_ERR_FORCE_FORMAT:
      clock_gettime(CLOCK_REALTIME, &format_time_start);
      /* This generally indicates that we've never formatted the flash,
       * the file system has to be formatted first before we can start using it.
       * It takes about 96 seconds to format a 15MB filesystem. */
      ret = SPIFFS_format(&_spiffs.spiffs);
      clock_gettime(CLOCK_REALTIME, &format_time_end);
      ts_subtract(&result, &format_time_end, &format_time_start);
      _spiffs.format_time_ms = result.tv_sec * ORDER_3 + result.tv_nsec / ORDER_6;
      _spiffs.just_formatted = true;

      if (SPIFFS_OK == ret)
      {
#if SPIFFS_CACHE
        ret = SPIFFS_mount(&_spiffs.spiffs, &cfg, _spiffs_work_buf, _spiffs_fds,
                           sizeof(_spiffs_fds), _spiffs_cache_buf, sizeof(_spiffs_cache_buf), 0);
#else
        ret = SPIFFS_mount(&_spiffs.spiffs, &cfg, _spiffs_work_buf, _spiffs_fds,
                           sizeof(_spiffs_fds), NULL, 0, 0);
#endif
      }

      break;

    case SPIFFS_OK:
      break;

    default:
      /* TODO: add error hanlding for any other cases */
      break;
  }

  clock_gettime(CLOCK_REALTIME, &mount_time_end);
  ts_subtract(&result, &mount_time_end, &mount_time_start);
  _spiffs.mount_time_ms = result.tv_sec * ORDER_3 + result.tv_nsec / ORDER_6;

  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : OSA_ERR;
}


/*!
 * @see spiffs_api.h
 */
int32_t spiffs_api_unmount(void)
{
  SPIFFS_unmount(&_spiffs.spiffs);
  return PHAL_FS_SUCCESS;
}


/*!
 * @see spiffs_api.h
 */
int32_t spiffs_api_get_fs_driver(phal_fs_api_t *fs)
{
  if (NULL == fs)
  {
    return OSA_ERR_INVALID_PARAM;
  }

  fs->fs_handle = &_spiffs.spiffs;
  fs->open = _spiffs_api_open;
  fs->close = _spiffs_api_close;
  fs->read = _spiffs_api_read;
  fs->write = _spiffs_api_write;
  fs->lseek = _spiffs_api_lseek;
  fs->stat = _spiffs_api_stat;
  fs->fstat = _spiffs_api_fstat;
  fs->fsync = _spiffs_api_fsync;
  fs->unlink = _spiffs_api_unlink;
  fs->truncate = _spiffs_api_truncate;
  fs->ftruncate = _spiffs_api_ftruncate;
  fs->rename = _spiffs_api_rename;
  fs->factory_reset = _spiffs_api_factory_reset;

  return PHAL_FS_SUCCESS;
}


/*!
 * @see spiffs_api.h
 */
bool spiffs_api_is_mounted(void)
{
  return 0 != SPIFFS_mounted(&_spiffs.spiffs);
}


/*!
 * @see spiffs_api.h
 */
uint32_t spiffs_api_get_mount_time_ms(void)
{
  return _spiffs.mount_time_ms;
}


/*!
 * @see spiffs_api.h
 */
uint32_t spiffs_api_get_format_time_ms(void)
{
  return _spiffs.format_time_ms;
}

/*!
 * @see spiffs_api.h
 */
uint32_t spiffs_api_get_fs_size(void)
{
  return SPIFFS_FLASH_SIZE;
}

/*!
 * @see spiffs_api.h
 */
bool spiffs_api_is_formatted(void)
{
  return _spiffs.just_formatted;
}

/*!
 * @see spiffs_api.h
 */
int32_t spiffs_api_format_on_next_boot(void)
{
  /* TODO: This is for development only. Exclude this in release builds. */
  spiffs_file file = SPIFFS_open(&_spiffs.spiffs, SPIFFS_FORCE_FORMAT_FILE,
                                 SPIFFS_CREAT, SPIFFS_UNUSED);

  if (file < 0)
  {
    return OSA_ERR;
  }

  s32_t ret = SPIFFS_close(&_spiffs.spiffs, file);
  return (SPIFFS_OK == ret) ? PHAL_FS_SUCCESS : OSA_ERR;
}

/*! @} */

#endif /* CA_API */
