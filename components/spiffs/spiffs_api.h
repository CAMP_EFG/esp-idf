// Copyright 2015-2017 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#ifndef CA_API /* ESP API */

#include <stdint.h>
#include <stddef.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "spiffs.h"
#include "esp_vfs.h"
#include "esp_compiler.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief SPIFFS definition structure
 */
typedef struct {
    spiffs *fs;                             /*!< Handle to the underlying SPIFFS */
    SemaphoreHandle_t lock;                 /*!< FS lock */
    const esp_partition_t* partition;       /*!< The partition on which SPIFFS is located */
    char base_path[ESP_VFS_PATH_MAX+1];     /*!< Mount point */
    bool by_label;                          /*!< Partition was mounted by label */
    spiffs_config cfg;                      /*!< SPIFFS Mount configuration */
    uint8_t *work;                          /*!< Work Buffer */
    uint8_t *fds;                           /*!< File Descriptor Buffer */
    uint32_t fds_sz;                        /*!< File Descriptor Buffer Length */
    uint8_t *cache;                         /*!< Cache Buffer */
    uint32_t cache_sz;                      /*!< Cache Buffer Length */
} esp_spiffs_t;

s32_t spiffs_api_read(spiffs *fs, uint32_t addr, uint32_t size, uint8_t *dst);

s32_t spiffs_api_write(spiffs *fs, uint32_t addr, uint32_t size, uint8_t *src);

s32_t spiffs_api_erase(spiffs *fs, uint32_t addr, uint32_t size);

void spiffs_api_check(spiffs *fs, spiffs_check_type type,
                            spiffs_check_report report, uint32_t arg1, uint32_t arg2);

#ifdef __cplusplus
}
#endif

#else /* CA_API */

#include "phal_fs.h"
#include "phal_fs_dfns.h"

/*! @defgroup SPIFFS_API
 * @ingroup SPIFFS
 * @{
 */


/*!
 * \brief  Mount the file system
 *
 * @note this may takes up to ~109 seconds if file system has never been formatted.
 *
 * @return RC_SUCCESS if success
 */
int32_t spiffs_api_mount(void);


/*!
 * \brief  Unmount the file system
 *
 * @note this must be called before powering down
 *
 * @return RC_SUCCESS if success
 */
int32_t spiffs_api_unmount(void);


/*!
 * \brief  Returns the fs_api driver
 *
 * @return RC_SUCCESS if success
 */
int32_t spiffs_api_get_fs_driver(phal_fs_api_t *fs);


/*!
 * \brief  Check if the file system is mounted.
 *
 * @return true if mounted
 */
bool spiffs_api_is_mounted(void);


/*!
 * \brief  Get the time to mount the file system
 *
 * @return time in ms
 */
uint32_t spiffs_api_get_mount_time_ms(void);


/*!
 * \brief  Get the time to format the file system if any
 *
 * @return time in ms
 */
uint32_t spiffs_api_get_format_time_ms(void);


/*!
 * \brief  Get the size of filesystem
 *
 * @return size in bytes
 */
uint32_t spiffs_api_get_fs_size(void);


/*!
 * \brief  Check if the file system is formatted.
 *
 * @return true if formatted
 */
bool spiffs_api_is_formatted(void);


/*!
 * \brief  Request to format the file system on next boot
 *
 * @return RC_SUCCESS if success
 */
int32_t spiffs_api_format_on_next_boot(void);

/*!
 *  @brief Install spiffs commands
 *
 * @param[in] install - install or uninstall the command table.
 */
void spiffs_install_cmd_table(bool install);

/*! @} */

#endif /* CA_API */
